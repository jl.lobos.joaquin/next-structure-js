import character from "./character";

const services = () => ({
  character: character(),
});

export default services;
