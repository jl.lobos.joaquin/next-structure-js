import services from "@/infraestructure/services";

const network = (client) => ({
  getAllCharacters: () => client.get(services().character.listCharactersURL),
});

export default network;
