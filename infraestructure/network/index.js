import axios from "axios";
import { authRequestInterceptor } from "./interceptors/requestInterceptor";
import { store } from "../../infraestructure/redux";
import { logout } from "../redux/slices/auth/authSlice";
import characterNetwork from "../network/character";

const httpConfig = {
  baseURL: "https://rickandmortyapi.com/api",
};

const http = axios.create(httpConfig);
const httpAuth = axios.create(httpConfig);

httpAuth.interceptors.request.use(authRequestInterceptor, (error) => {
  console.log("ERROR", error);
  return Promise.reject(error);
});

httpAuth.interceptors.response.use(
  (response) => {
    return response;
  },
  (error) => {
    if (error.response && error.response.status === 401) {
      console.log("ERROR 401", error.response);
      store.dispatch(logout());
    }
    return Promise.reject(error);
  }
);

const character = characterNetwork(http);

export const getAllCharacters = character.getAllCharacters;
