import { cookieStorage } from "@/infraestructure/helpers/cookieStorage";

export const authRequestInterceptor = (request) => {
  const token = cookieStorage.getItem("accessToken");

  if (token) {
    request.headers = { Authorization: `Bearer ${token}` };
  }
  return request;
};
