import { configureStore } from "@reduxjs/toolkit";
import { persistStore } from "redux-persist";
import { createWrapper } from "next-redux-wrapper";
import createSagaMiddleware from "redux-saga";
import rootSaga from "./sagas";
import rootSlices from "./slices/slices";

const sagaMiddleware = createSagaMiddleware();

export const store = configureStore({
  reducer: rootSlices,
  middleware: [sagaMiddleware],
  // eslint-disable-next-line no-undef
  devTools: process.env.NODE_ENV !== "production",
});

// sagaMiddleware.run(rootSaga);

export const wrapper = createWrapper(store, { debug: true }); // Set debug to false in production
export const persistor = persistStore(store);
