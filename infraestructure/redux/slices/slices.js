import { combineReducers } from "redux";
import { persistReducer, createTransform } from "redux-persist";
import CryptoJS from "crypto-js";
import authSlice from "./auth/authSlice";
import userSlice from "./user/userSlice";
import { cookieStorage } from "../../helpers/cookieStorage";

const encryptor = createTransform(
  (inboundState) => {
    // encriptar el estado antes de almacenarlo
    const ciphertext = CryptoJS.AES.encrypt(
      JSON.stringify(inboundState),
      "secret key 123"
    );
    return ciphertext.toString();
  },
  (outboundState) => {
    // desencriptar el estado antes de usarlo
    const bytes = CryptoJS.AES.decrypt(outboundState, "secret key 123");
    const originalText = bytes.toString(CryptoJS.enc.Utf8);
    return JSON.parse(originalText);
  }
);

//
const authPersistConfig = {
  key: "auth",
  storage: cookieStorage,
  transforms: [encryptor],
};

const persistedAuthReducer = persistReducer(authPersistConfig, authSlice);

export default combineReducers({
  auth: persistedAuthReducer,
  user: userSlice,
});
