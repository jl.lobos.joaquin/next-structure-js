import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  users: [],
  searchTerm: "",
  sort: "asc",
};

const userSlice = createSlice({
  name: "user",
  initialState,
  reducers: {
    setUsers: (state, action) => {
      state.users = action.payload;
    },
    setSearchTerm: (state, action) => {
      state.searchTerm = action.payload;
    },
    setSort: (state, action) => {
      state.sort = action.payload;
    },
  },
});

export const { setUsers, setSearchTerm, setSort } = userSlice.actions;
export default userSlice.reducer;
