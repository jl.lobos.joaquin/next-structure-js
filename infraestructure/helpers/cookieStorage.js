import Cookies from "js-cookie";

export const cookieStorage = {
  getItem: (key) => {
    return new Promise((resolve) => {
      const cookie = Cookies.get(key);
      resolve(cookie ? JSON.parse(cookie) : null);
    });
  },
  setItem: (key, value) => {
    return new Promise((resolve) => {
      Cookies.set(key, JSON.stringify(value));
      resolve();
    });
  },
  removeItem: (key) => {
    return new Promise((resolve) => {
      Cookies.remove(key);
      resolve();
    });
  },
};
