import fetchCharacters from "@/app/api/apiRickAndMorty";
import { useQuery } from "@tanstack/react-query";

const useCharacters = () => {
  const {
    data: characters,
    isLoading,
    isError,
    error,
    isFetching,
  } = useQuery({
    queryKey: ["characters"],
    queryFn: fetchCharacters,
    retry: 1,
  });

  return { characters, isLoading, isFetching, isError, error };
};

export default useCharacters;
