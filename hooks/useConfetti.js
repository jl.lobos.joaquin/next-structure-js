import { useEffect, useState } from "react";
import Confetti from "confetti-js";

function useConfetti(target) {
  const [confetti, setConfetti] = useState(null);

  useEffect(() => {
    if (target) {
      const confettiSettings = {
        target,
        props: [
          { type: "svg", src: "letterX.svg" },
          { type: "svg", src: "letterY.svg" },
        ],
        colors: [[0, 255, 0]],
      };
      const confettiInstance = new Confetti(confettiSettings);
      setConfetti(confettiInstance);
      return () => confettiInstance.clear(); // Limpiar en desmontaje
    }
  }, [target]);

  const startConfetti = () => {
    if (confetti) {
      confetti.render();
    }
  };

  return { startConfetti };
}

export { useConfetti };
