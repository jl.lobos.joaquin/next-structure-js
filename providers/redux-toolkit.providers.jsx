"use client";

import { store, persistor } from "@/infraestructure/redux";
import { PersistGate } from "redux-persist/integration/react";
import { Provider } from "react-redux";

export default function StoreReduxProviders({ children }) {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        {children}
      </PersistGate>
    </Provider>
  );
}
