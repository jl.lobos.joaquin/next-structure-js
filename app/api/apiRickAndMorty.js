import { getAllCharacters } from "@/infraestructure/network";

const fetchCharacters = async () => {
  try {
    const res = await getAllCharacters();
    return res.data.results;
  } catch (error) {
    console.log("ERROR", error);
    throw new Error("Error fetching characters");
  }
};

export default fetchCharacters;
