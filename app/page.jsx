import ToggleTheme from "@/components/client/ToggleTheme";
import CharacterList from "@/components/client/character/CharacterList";

export default function Home() {
  return (
    <main>
      <ToggleTheme />
      <div className="flex justify-center flex-col items-center  min-h-screen overflow-hidden ">
        <div>
          <p className="text-6xl">
            List of <span className="text-red-500">Characters</span>
          </p>
        </div>
        <div>
          <CharacterList />
        </div>
      </div>
    </main>
  );
}
