import TestProviders from "@/providers/test.providers";

export default function User() {
  return (
    <main>
      <TestProviders>
        <h1>User</h1>
      </TestProviders>
    </main>
  );
}
