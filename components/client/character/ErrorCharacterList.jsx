export default function ErrorCharacterList({ error }) {
  return (
    <div className="relative flex items-center justify-center min-h-screen overflow-hidden">
      {error.message}
    </div>
  );
}
