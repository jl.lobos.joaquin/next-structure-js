export default function NotCharacterList() {
  return (
    <div className="relative flex items-center justify-center min-h-screen overflow-hidden">
      No characters
    </div>
  );
}
