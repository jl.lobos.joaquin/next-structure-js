"use client";

import useCharacters from "@/hooks/useCharacters";
import NotCharacterList from "./NotCharacterList";
import ErrorCharacterList from "./ErrorCharacterList";
import LoadingCharacterList from "./LoadingCharacterList";

export default function CharacterList() {
  const { characters, error, isError, isLoading, isFetching } = useCharacters();

  if (isLoading || isFetching) {
    return <LoadingCharacterList />;
  }

  if (isError || error) {
    return <ErrorCharacterList error={error} />;
  }

  if (!characters) {
    return <NotCharacterList />;
  }

  return (
    <ul className="py-10">
      {characters?.map((character) => (
        <li key={character.id}>
          <p className="text-4xl"> {character.name}</p>
        </li>
      ))}
    </ul>
  );
}
