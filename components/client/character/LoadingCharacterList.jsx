export default function LoadingCharacterList() {
  return (
    <div className="relative flex items-center justify-center min-h-screen overflow-hidden">
      Loading...
    </div>
  );
}
